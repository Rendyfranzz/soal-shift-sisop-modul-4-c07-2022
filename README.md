# soal-shift-sisop-modul-4-C07-2022

## Anggota Kelompok ##

NRP | NAMA
------------- | -------------
5025201039  | Abd. Wahid
5025201056  | Rendi Dwi Francisko
5025201239  | Kadek Ari Dharmika


Pada awal program kami declare 3 variable yang akan menentukan tipe enkripsi yang sesuai dengan soal
```
static int ANIMEKU = 0;
static int IAN = 1;
static int NAM_DO_SAQ = 2;
```
`ANIMEKU` untuk soal 1, `IAN` untuk soal 2, `NAM_DO_SAQ` untuk soal 3

### Read Directory Function
```
static int fuse_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
        char filePath[1024];
        int encryptionType = decodePath(filePath, path);
       
        DIR *directory;
        struct dirent *dirData;
        struct stat st;
        char fileName[1024];
 
        (void) offset;
        (void) fi;
       
        directory = opendir(filePath);
       
        if (directory == NULL)
        {
                return -errno;
        }      
       
        while((dirData = readdir(directory)) != NULL)
        {
                memset(&st, 0, sizeof(st));
               
                st.st_ino = dirData->d_ino;
                st.st_mode = dirData->d_type << 12;
               
               
                strcpy(fileName, filePath);
                strcat(fileName, "/");
                strcat(fileName, dirData->d_name);
                if (isRegularFile(fileName))
                {      
                        strcpy(fileName, dirData->d_name);
                        encryptFile(fileName, 0, strlen(fileName), encryptionType);
                }
                else
                {
                        strcpy(fileName, dirData->d_name);
                        if (encryptionType != NAM_DO_SAQ)
                        {
                                encryptText(fileName, 0, strlen(fileName), encryptionType);
                        }
                }
 
                if (filler(buf, fileName, &st, 0)) break;
        }
       
        closedir(directory);
       
        return 0;
}
```
Fungsi `fuse_readdir` akan membaca directory dengan library `dirent.h`.
1. Di awal fungsi akan menentukan `encryptionType` apakah itu untuk No. 1, 2, atau 3.
2. Program akan mulai membaca directory.
3. Jika merupaka regular file name, maka file akan dienkripsi dengan memanggil fungsi `encryptFile`.
4. Close directory.


### No.1
Anya adalah adalah seorang programmer wibu yang suka mengoleksi anime. Dia sangat senang membuat program yang dikolaborasikan dengan anime. Suatu hari, Anya sedang ingin membuat system dengan ketentuan berikut:
	
A. Semua direktori dengan awalan “Animeku_” akan terencode dengan ketentuan semua file yang terdapat huruf besar akan ter encode dengan atbash cipher dan huruf kecil akan terencode dengan rot13
	
	Contoh : 
	“Animeku_/anya_FORGER.txt” → “Animeku_/naln_ULITVI.txt”
B. Semua direktori di-rename dengan awalan “Animeku_”, maka direktori tersebut akan menjadi direktori ter-encode dengan ketentuan sama dengan 1a.
C. Apabila direktori yang terenkripsi di-rename menjadi tidak ter-encode, maka isi direktori tersebut akan terdecode.
D. Setiap data yang terencode akan masuk dalam file “Wibu.log” 
Contoh isi: 
RENAME terenkripsi /home/[USER]/Downloads/hai --> /home/[USER]/Downloads/Animeku_hebat 
RENAME terdecode /home/[USER]/Downloads/Animeku_ --> /home/[USER]/Downloads/Coba
E. Metode encode pada suatu direktori juga berlaku terhadap direktori yang ada di dalamnya.(rekursif)

##### Encode directory
```
void encryptText(char *str, int startIndex, int endIndex, int encryptionType)
{
        int vigenereIndex = 0;
        if (encryptionType != NAM_DO_SAQ)
        {
                for (int i = startIndex; i < endIndex; i++)
                {
                        if (encryptionType == ANIMEKU)
                        {
                                if (str[i] >= 'a' && str[i] <= 'z')
                                {
                                        str[i] = rot13(str[i]);
                                }
                                else if (str[i] >= 'A' && str[i] <= 'Z')
                                {
                                        str[i] = atbashCipher(str[i]);
                                }
                        }
                        else if (encryptionType == IAN)
                        {
                                str[i] = vigenereCipherEncode(str[i], vigenereIndex);
                                vigenereIndex++;
                        }
                }
        }
        .
        .
        .
        .
        .
        .
        .
}
```
Setelah program membaca file directory dengan fungsi `fuse_readdir`, akan dipanggil pula fungsi `encryptText` dari `fuse_readdir`. Nama file akan di encode dengan mamnggil fungsi `rot13` untuk lowercase dan `atbashCipher` untuk uppercase.

##### Rename file directory `Animeku_`
```
static int fuse_rename(const char *old, const char *new)
{
        char filePathOld[1024];
        char filePathNew[1024];
        decodePath(filePathOld, old);
        decodeDirectoryForRename(filePathNew, new);
       
        char fileNameOld[1024];
        char fileNameNew[1024];
       
        getFileNameFromPath(fileNameOld, old);
        getFileNameFromPath(fileNameNew, new);
       
        FILE *fileWriterWibuLog = fopen(wibuLogPath, "a");
        if (strncmp(fileNameOld, "Animeku_", 8) == 0 && strncmp(fileNameNew, "Animeku_", 8) != 0)
        {
                fprintf(fileWriterWibuLog, "RENAME terdecode %s --> %s\n", old, new);
        }
        else if (strncmp(fileNameOld, "Animeku_", 8) != 0 && strncmp(fileNameNew, "Animeku_", 8) == 0)
        {
                fprintf(fileWriterWibuLog, "RENAME terenkripsi %s --> %s\n", old, new);
        }
        fclose(fileWriterWibuLog);
 
        writeLog("INFO", "RENAME", old, new);
       
        return rename(filePathOld, filePathNew);
}
 ```

Fungsi akan decode path dan directory dengan memanggil fungsi `decodePath` dan `decodeDirectoryForRename`. Jika berhasil maka fungsi akan write file `Wibu.log` yang terseimpan dalam `wibuLogPath` dengan membedakan antara yang terdecode dan ternkripsi.


### No.2
Saat Anya sedang sibuk mengerjakan programnya, tiba-tiba Innu datang ke rumah Anya untuk mengembalikan barang yang dipinjamnya. Innu adalah programmer jenius sekaligus teman Anya. Ia melihat program yang dibuat oleh Anya dan membantu Anya untuk menambahkan fitur pada programnya dengan ketentuan sebagai berikut : 
A. Jika suatu direktori dibuat dengan awalan “IAN_[nama]”, maka seluruh isi dari direktori tersebut akan terencode dengan algoritma Vigenere Cipher dengan key “INNUGANTENG” (Case-sensitive, Vigenere).
B. Jika suatu direktori di rename dengan “IAN_[nama]”, maka seluruh isi dari direktori tersebut akan terencode seperti pada no. 2a.
C. Apabila nama direktori dihilangkan “IAN_”, maka isi dari direktori tersebut akan terdecode berdasarkan nama aslinya.
D. Untuk memudahkan dalam memonitor kegiatan yang dilakukan pada filesystem yang telah dibuat, ia membuat log system pada direktori “/home/[user]/hayolongapain_[kelompok].log”. Dimana log ini akan menyimpan daftar perintah system call yang telah dijalankan pada filesystem.
E. Karena Innu adalah seorang perfeksionis, ia membagi isi dari log systemnya menjadi 2 level, yaitu level INFO dan WARNING. Untuk log level WARNING, digunakan untuk mencatat syscall rmdir dan unlink. Sisanya, akan dicatat pada level INFO dengan format sebagai berikut : 

[Level]::[dd][mm][yyyy]-[HH]:[MM]:[SS]::[CMD]::[DESC :: DESC]


##### Encode directory  
```
void encryptFile(char *str, int startIndex, int endIndex, int encryptionType)
{
        .
        .
        .
        .
        .
        else if (encryptionType == IAN)
        {
                encryptText(str, startIndex, endIndex, encryptionType);
        }
}
```
Pada fungsi `encryptFile` ada else if pada variabel `encryptionType` yang akan membedakan encrypt file IAN_ yang selanjutnya akan memanggil fungsi `encryptText`

```
void encryptText(char *str, int startIndex, int endIndex, int encryptionType)
{
        .
        .
        .
        .
        .

                        else if (encryptionType == IAN)
                        {
                                str[i] = vigenereCipherEncode(str[i], vigenereIndex);
                                vigenereIndex++;
                        }
        .
        .
        .
}
```
Fungsi `encryptText` akan mengencrypt file path yang ada di variabel `str` dengan memanggil fungsi `vigenereCipherEncode`.

```
char vigenereCipherEncode(char c, int i)
{
        int addition = vigenereKey[i % vigenereKeyLength] - 'A';
        if (c >= 'a' && c <= 'z')
        {
                return (c - 'a' + addition) % 26 + 'a';
        }
        else if (c >= 'A' && c <= 'Z')
        {
                return (c - 'A' + addition) % 26 + 'A';
        }
        return c;
}
```
Fungsi `vigenereCipherEncode` akan mengencode isi directory dengan algoritma Vigenere Cipher dimana keynya di assign di variabel `vigenereKey`.

##### System Call dalam Log
```
void writeLog(char level[], char operand[], char arg1[], char arg2[])
{
        FILE *fileWriterInnuLogPath = fopen(innuLogPath, "a");
        time_t currentTime = time(NULL);
        struct tm currentLocalTime = *localtime(&currentTime);
        char logText[256];
       
        sprintf(logText, "%s::%02d%02d%04d-%02d:%02d:%02d::%s",
                level,
                currentLocalTime.tm_mday,
                currentLocalTime.tm_mon,
                currentLocalTime.tm_year + 1900,
                currentLocalTime.tm_hour,
                currentLocalTime.tm_min,
                currentLocalTime.tm_sec,
                operand
        );
 
        if (strlen(arg1) != 0)
        {
                strcat(logText, "::");
                strcat(logText, arg1);
        }
 
        if (strlen(arg2) != 0)
        {
                strcat(logText, "::");
                strcat(logText, arg2);
        }
        fprintf(fileWriterInnuLogPath, "%s\n", logText);
       
        fclose(fileWriterInnuLogPath);
}
```
Pertama file akan diopen dimana nama file disimpan dalam variabel `innuLogPath`. Dengan library `time.h`, assign struct yang menyimpan waktu system call yang terjadi dan write dalam log yang telah dibuka.



### No.3
Ishaq adalah seseorang yang terkenal di kalangan anak informatika seluruh indonesia. Ia memiliki teman yang bernama innu dan anya, lalu ishaq bertemu dengan mereka dan melihat program yang mereka berdua kerjakan  sehingga ia tidak mau kalah dengan innu untuk membantu anya dengan menambah fitur yang ada pada programnya dengan ketentuan :
A. Jika suatu direktori dibuat dengan awalan “nam_do-saq_”, maka direktori tersebut akan menjadi sebuah direktori spesial
B. Jika suatu direktori di-rename dengan memberi awalan “nam_do-saq_”,maka direktori tersebut akan menjadi sebuah direktori spesial.
C. Apabila direktori yang terenkripsi di-rename dengan menghapus “nam_do-saq_” pada bagian awal nama folder maka direktori tersebut menjadi direktori normal.
D. Direktori spesial adalah direktori yang mengembalikan enkripsi/encoding pada direktori “Animeku_” maupun “IAN_” namun masing masing aturan mereka tetap berjalan pada direktori di dalamnya (sifat recursive “Animeku_” dan “IAN_” tetap berjalan pada subdirektori).
E. Pada direktori spesial semua nama file (tidak termasuk ekstensi) pada fuse akan berubah menjadi uppercase insensitive dan diberi ekstensi baru berupa nilai desimal dari biner perbedaan namanya.


##### Menamakan file sesuai dengan nama di FUSE
Sebelum masuk fungsi di bawah nantinya direktori dengan awalan “nam_do-saq_” akan dibedakan `encryptionType` nya dengan fungsi `decode` saat program membaca directory pada fungsi `fuse_readdir`
```
void decryptFile(char *str, int startIndex, int endIndex, int encryptionType)
{
        .
        .
        .
        .
        .
        .
        else if (encryptionType == NAM_DO_SAQ)
        {
                int fileExtensionPosDecimalCode = endIndex - 1;
                while(fileExtensionPosDecimalCode >= startIndex && str[fileExtensionPosDecimalCode] != '.')
                {
                        fileExtensionPosDecimalCode--;
                }
               
                fileExtensionPos = fileExtensionPosDecimalCode - 1;
                while(fileExtensionPos >= startIndex && str[fileExtensionPos] != '.')
                {
                        fileExtensionPos--;
                }
               
                if (fileExtensionPosDecimalCode < startIndex)
                {
                        decryptText(str, startIndex, -1, encryptionType);
                }
                else if (fileExtensionPos < 0)
                {
                        decryptText(str, fileExtensionPosDecimalCode - 1, fileExtensionPosDecimalCode + 1, encryptionType);
                }
                else
                {
                        decryptText(str, fileExtensionPos - 1, fileExtensionPosDecimalCode + 1, encryptionType);
                        str[fileExtensionPosDecimalCode] = '\0';
                }
        }
        .
        .
        .
        .
}
```
Saat `encryptionType` yang diassign dalam fungsi `decryptFile` adalah `NAM_DO_SAQ` maka fungsi akan:
1. Membuat variabel yang akan menampung nama file yang akan dicari perubahannya pada FUSE
2. decrypt nama file dengan fungsi `decryptText`

```
void decryptText(char *str, int startIndex, int endIndex, int encryptionType)
{
        .
        .
        .
        .
        .
        .
        else if (encryptionType == NAM_DO_SAQ)
        {
                if (endIndex != -1)
                {
                        char binary[128];
                        convertDecimalToBinary(binary, str + endIndex);
                        int length = strlen(binary);
                        for (int i = length-1; i >= 0; i--)
                        {
                                if (binary[i] == '1')
                                {
                                        str[startIndex] = str[startIndex] - 'A' + 'a';
                                }
                                startIndex--;
                        }
                }
        }
}
```
Fungsi `decryptText` akan mengubah nama file dengan uppercase 

##### E. Menambahkan ekstensi nilai desimal dari biner perubahan nama file
```
static int fuse_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
                .
                .
                .
                .
                if (isRegularFile(fileName))
                {      
                        strcpy(fileName, dirData->d_name);
                        encryptFile(fileName, 0, strlen(fileName), encryptionType);
                }
                else
                {
                        strcpy(fileName, dirData->d_name);
                        if (encryptionType != NAM_DO_SAQ)
                        {
                                encryptText(fileName, 0, strlen(fileName), encryptionType);
                        }
                }
                .
                .
                .
                .
}
```
Saat program membaca directory dengan fungsi `fuse_readdir`, file-file yang dibaca akan di enkripsi dengan memanggil fungsi `encryptFile`

```
void encryptFile(char *str, int startIndex, int endIndex, int encryptionType)
{
        int fileExtensionPos = endIndex - 1;
        if (encryptionType == ANIMEKU || encryptionType == NAM_DO_SAQ)
        {
                while(fileExtensionPos >= startIndex && str[fileExtensionPos] != '.')
                {
                        fileExtensionPos--;
                }
                if (fileExtensionPos < startIndex)
                {
                        encryptText(str, startIndex, endIndex, encryptionType);
                }
                else
                {
                        encryptText(str, startIndex, fileExtensionPos, encryptionType);
                }
        }
        else if (encryptionType == IAN)
        {
                encryptText(str, startIndex, endIndex, encryptionType);
        }
}
``` 
Fungsi `encryptFile` akan menspesifikan index nama file yang akan diencrypt dan memanggil fungsi `encryptText` untuk mengenkripsi nama file dengan start dan end indexnya.

```
void encryptText(char *str, int startIndex, int endIndex, int encryptionType)
{
        .
        .
        .
        .
        .
        else
        {
                char biner[endIndex - startIndex + 1];
                for (int i = 0; i < endIndex - startIndex; i++)
                {
                        if (str[i] >= 'a' && str[i] <= 'z')
                        {
                                str[i] = str[i] - 'a' + 'A';
                                biner[i] = '1';
                        }
                        else
                        {
                                biner[i] = '0';
                        }
                }
                biner[endIndex - startIndex] = '\0';
                char decimal[128];
                convertBinerToDecimal(decimal, biner);
                strcat(str, ".");
                strcat(str, decimal);
        }
}
```
Fungsi `encryptText` ini nantinya yang akan mendapatkan biner perbedaan nama file di fuse dan menambahkan decimalnya pada nama file.

### Kendala dan Error
1. Program belum dapat dijalankan semestinya
2. Pengerjaan terkendala karena balik mudik ke Surabaya
3. Soal  yang lumayan sulit
